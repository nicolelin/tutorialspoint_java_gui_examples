package EX11_PieChart;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import javax.swing.JComponent;
import javax.swing.JFrame;

class Part {
    double value;
    Color color;

    public Part(double value, Color color) {
        this.value = value;
        this.color = color;
    }
}

public class Panel {
    public static void main(String[] argv) {
        JFrame frame = new JFrame();
        frame.getContentPane().add(new MyComponent());
        frame.setSize(300, 200);
        frame.setVisible(true);
    }
}